{-# LANGUAGE QuasiQuotes #-}
module Handler.Profile where

import           Database.Esqueleto as E
import           Import             as I
import           Yesod.Auth

getProfileR :: Handler Html
getProfileR = do
  maui <- maybeAuthId
  case maui of
    Just uid -> do
      Just (Entity _ user) <- runDB $ selectFirst [UserId I.==. uid] []
      favs <- runDB $ selectList [FavoritesUserId I.==. uid] []

      let query = E.select $ E.from $ \(f `InnerJoin` v) -> do
                   on (f ^. FavoritesVideoId E.==. v ^. VideoId)
                   where_ (f ^. FavoritesUserId E.==. (val uid))
                   orderBy [asc (f ^. FavoritesCreatedAt)]
                   return v

      favVideos <- runDB query
      let fullFavs = zip favVideos favs

      let subsQuery = E.select $ E.from $ \(t `InnerJoin` ts) -> do
                        on (ts ^. TagSubscriptionTagId E.==. t ^. TagId)
                        where_ (ts ^. TagSubscriptionSubscriberId E.==. (val uid))
                        orderBy [desc (ts ^. TagSubscriptionCreatedAt)]
                        return t

      subsTags <- runDB subsQuery
      -- let subTagsIds = map (\(Entity eid _) -> eid) subsTags

      let subsPoolsQuery = E.select $ E.from $ \(p `InnerJoin` ps) -> do
                             on (ps ^. PoolSubscriptionPoolId E.==. p ^. PoolId)
                             where_ (ps ^. PoolSubscriptionSubscriberId E.==. (val uid))
                             orderBy [desc (ps ^. PoolSubscriptionCreatedAt)]
                             return p
      subsPools <- runDB subsPoolsQuery

      defaultLayout $(widgetFile "profile")

    Nothing -> defaultLayout
              [whamlet|
                <p>
                  Anonymous user
              |]
