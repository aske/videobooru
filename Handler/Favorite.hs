module Handler.Favorite where

import           Data.Time
import           Import
import           Yesod.Auth

getFavoriteR :: VideoId -> Handler Html
getFavoriteR videoId = do
  video <- runDB $ get404 videoId
  Just aui <- maybeAuthId
  prevFav <- runDB $ selectFirst [FavoritesUserId ==. aui, FavoritesVideoId ==. videoId] []
  curTime <- liftIO getCurrentTime
  case prevFav of
    Nothing -> do
      _ <- runDB $ update videoId [VideoFavoritesCount +=. 1]
      _ <- runDB $ insert $ Favorites aui videoId curTime
      redirect $ VideoR videoId
    Just _ -> redirect $ VideoR videoId
