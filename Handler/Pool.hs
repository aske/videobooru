{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Handler.Pool where

import           CustomWidgets
import           Data.Maybe
import           Database.Esqueleto as E
import           FixedPaginator
import           Import             as I
import           Yesod.Auth


-- poolSidebar :: PoolId -> Widget -> Widget
poolSidebar poolWidget maui poolId = do
  [whamlet|
      <div>
        <aside .sidebar>
          $maybe _ <- maui
            <a href=@{PoolManageR poolId}>
              <button type="link" .btn>Manage users
            <a href=@{PoolAddVideoR poolId}>
              <button type="link" .btn>Manage videos
          <section>
            <h2>Pool
            ^{poolWidget}
  |]

poolInfoBar :: Pool -> Widget
poolInfoBar pool =
  [whamlet|
    <article>
      #{poolName pool}
      <span .badge>#{poolVideoCount pool}
    <article>
    $maybe Textarea desc <- poolDescription pool
      #{desc}
    $nothing
      No description
  |]

getPoolR :: PoolId -> Handler Html
getPoolR poolId = do
  pool <- runDB $ get404 poolId
  let query = E.select $ E.from $ \(v `InnerJoin` pv) -> do
               on ((v ^. VideoId) E.==. (pv ^. PoolVideosVideoId))
               where_ (pv ^. PoolVideosPoolId E.==. (val poolId))
               orderBy [asc (v ^. VideoCreatedAt)]
               return v

  videos' <- runDB query
  muid <- maybeAuthId
  (videos, paginWidget) <- runDB $ paginate 9 videos'
  case muid of
    Just uid -> do
      subs <- runDB $ selectFirst [ PoolSubscriptionPoolId I.==. poolId
                                 , PoolSubscriptionSubscriberId I.==. uid] []
      let alreadySubscribed = isJust subs
      defaultLayout $(widgetFile "pool")

    Nothing -> do
      let alreadySubscribed = False
      defaultLayout $(widgetFile "pool")
