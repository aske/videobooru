module Handler.Just where

import           Data.Text   (pack)
import           Data.Time
import           Import
import           Network.Wai

getJustR :: Handler Html
getJustR = do
  now <- liftIO getCurrentTime
  setMessage $ toHtml $ "You previouslt visited at: " ++ show now
  sess <- getSession
  mname <- lookupSession "name"
  ip <- getRemoteIp
  liftIO $ print ip
  case mname of
       Nothing -> do
               setUltDestCurrent
               setMessage "Tell us how to greet you"
               runDB $ do curTime <- liftIO getCurrentTime
                          tz <- pack . show <$> liftIO getCurrentTimeZone
                          pingpongId <- insert $ User "admin" "12345" (Just "johnathan.joestar@gmail.com") False tz curTime (Just [])
                          sharklaserId <- insert $ User "laser" "12345" (Just "wpxixutx@sharklasers.com") False tz curTime (Just [])
                          bananaId <- insert $ User "Banana man" "password" Nothing False tz curTime (Just [])

                          _ <- insert $ User "Banana man2" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man3" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man4" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man5" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man6" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man7" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man8" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man9" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man12" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man13" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man14" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man15" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man16" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man17" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man18" "password" Nothing False tz curTime (Just [])
                          _ <- insert $ User "Banana man19" "password" Nothing False tz curTime (Just [])

                          curTime' <- liftIO getCurrentTime
                          pantiesId <- insert $ Tag "pancakes" 9000 curTime pingpongId Nothing curTime pingpongId
                          clothesId <- insert $ Tag "clothes" 4 curTime pingpongId Nothing curTime' pingpongId
                          curTime'' <- liftIO getCurrentTime
                          stockingsId <- insert $ Tag "swords" 88 curTime bananaId Nothing curTime'' bananaId
                          haskellId <- insert $ Tag "haskell" 300 curTime bananaId Nothing curTime'' pingpongId

                          -- v1 <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- v2 <- insert $ Video 0 0 0 pingpongId curTime VActive "Preview - 01 Before my body is dry - DON'T LOSE YOUR WAY" False (Just VPng) (Just "klkost.png") VWebm 300 "キルラキル Kill la Kill OST Preview - 01 Before my body is dry - DON'T LOSE YOUR WAY.webm" 1 [pantiesId, clothesId] 2 (Just . Textarea $ "that's klk preview") False

                          -- v3 <- insert $ Video 0 0 0 pingpongId curTime VActive "ルラキル Kill la Kill OST Preview - 02 goriLLA蛇L" False (Just VPng) (Just "klkost.png") VWebm 300 "キルラキル Kill la Kill OST Preview - 02 goriLLA蛇L.webm" 1 [pantiesId, clothesId] 2 (Just . Textarea $ "that's klk preview") False

                          -- v4 <- insert $ Video 0 0 0 pingpongId curTime VActive "キルラキル Kill la Kill OST Preview - 03 犬Kあ3L - PING PONG CIRCULATE" False (Just VPng) (Just "klkost.png") VWebm 300 "キルラキル Kill la Kill OST Preview - 03 犬Kあ3L - PING PONG CIRCULATE.webm" 1 [pantiesId, clothesId] 2 (Just . Textarea $ "that's klk preview") False

                          -- v5 <- insert $ Video 0 0 0 pingpongId curTime VActive "キルラキル Kill la Kill OST Preview - 04 Blumenkranz" False (Just VPng) (Just "klkost.png") VWebm 300 "キルラキル Kill la Kill OST Preview - 04 Blumenkranz.webm" 1 [pantiesId, clothesId] 2 (Just . Textarea $ "that's klk preview") False

                          -- v6 <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False


                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 1" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 2" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 3" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 4" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 5" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 6" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 7" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 pingpongId curTime VActive "Panty&Stocking theme copy 8" False (Just VJpg) (Just "panty-stocking-pic.jpeg") VWebm 300 "panty-stocking.webm" 1 [pantiesId, stockingsId] 2 (Just . Textarea $ "panty and stocking theme") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 1" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 2" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 3" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 4" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 5" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 6" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 7" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          -- _ <- insert $ Video 0 0 0 bananaId curTime VActive "Haskell is useless copy 8" False (Just VPng) (Just "haskell-logo.png") VWebm 300 "haskell-useless.webm" 1 [haskellId] 2 (Just . Textarea $ "jokes from spj") False

                          liftIO $ print pingpongId
               redirect SetGreetingR
       Just name -> do
                 users <- runDB $ selectList [] [Asc UserName]
                 liftIO $ print users
                 defaultLayout $ do
                   setTitle "Just "
                   $(widgetFile "just")

postJustR :: Handler ()
postJustR = do
  (key, mval) <- runInputPost $ (,) <$> ireq textField "key" <*> iopt textField "val"
  case mval of
    Nothing -> deleteSession key
    Just val -> setSession key val
  liftIO $ print (key, mval)
  redirect JustR

