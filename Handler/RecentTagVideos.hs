{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Handler.RecentTagVideos where

import           CustomWidgets
import           Database.Esqueleto as E
import           FixedPaginator
import           Import
import           Yesod.Auth

getRecentTagVideosR :: TagId -> Handler Html
getRecentTagVideosR tagId = do
  tag <- runDB $ get404 tagId
  let query = E.select $ E.from $ \(v `InnerJoin` vt) -> do
               on ((v ^. VideoId) E.==. (vt ^. VideoTagVideoId))
               where_ (vt ^. VideoTagTagId E.==. (val tagId))
               orderBy [asc (v ^. VideoCreatedAt)]
               return v

  videos' <- runDB query
  muid <- maybeAuthId
  (videos, paginWidget) <- runDB $ paginate 9 videos'
  subscriptionsLayout $(widgetFile "recent-tag-videos")
