{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Handler.Tag where

import           CustomWidgets
import           Data.Maybe
import           Database.Esqueleto as E
import           FixedPaginator
import           Import             as I
import           Yesod.Auth

tagInfoBar :: Tag -> Widget
tagInfoBar tag =
  [whamlet|
    <article>
      #{tagName tag}
      <span .badge>#{tagVideoCount tag}
    <article>
    $maybe Textarea desc <- tagDescription tag
      #{desc}
    $nothing
      No description
  |]

getTagR :: TagId -> Handler Html
getTagR tagId = do
  tag <- runDB $ get404 tagId
  let query = E.select $ E.from $ \(v `InnerJoin` vt) -> do
               on ((v ^. VideoId) E.==. (vt ^. VideoTagVideoId))
               where_ (vt ^. VideoTagTagId E.==. (val tagId))
               orderBy [asc (v ^. VideoCreatedAt)]
               return v

  videos' <- runDB query
  muid <- maybeAuthId
  (videos, paginWidget) <- runDB $ paginate 9 videos'
  case muid of
    Just uid -> do
      subs <- runDB $ selectFirst [ TagSubscriptionTagId I.==. tagId
                                 , TagSubscriptionSubscriberId I.==. uid] []
      let alreadySubscribed = isJust subs
      defaultLayout $(widgetFile "tag")

    Nothing -> do
      let alreadySubscribed = False
      defaultLayout $(widgetFile "tag")
