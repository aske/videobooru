{-# LANGUAGE TemplateHaskell #-}
module Handler.Upload where

import           Control.Applicative  ((<$>), (<*>))
import qualified Data.ByteString.Lazy as DBL
import           Data.Conduit
import           Data.Conduit.List    (consume)
import qualified Data.List            as L
import           Data.Maybe
import           Data.Text            (unpack)
import qualified Data.Text            as T
import           Data.Time
import           Import
import           System.Directory     (doesFileExist, removeFile)
import           System.FilePath
import           Yesod.Auth

imageUploadDir :: FilePath
imageUploadDir = "static/preview"

videoUploadDir :: FilePath
videoUploadDir = "static/video"

uploadForm :: Form (FileInfo, FileInfo, Text, Text, Maybe Textarea, UTCTime)
uploadForm = renderBootstrap $ (,,,,,)
    <$> fileAFormReq "Preview file"
    <*> fileAFormReq "Video file"
    <*> areq textField "Video title" Nothing
    <*> areq textField "Tags" Nothing
    <*> aopt textareaField "Video description" Nothing
    <*> lift (liftIO getCurrentTime)
    -- where
    --   tagsField = textField

getUploadR :: Handler Html
getUploadR = do
  mmsg <- getMessage
  (widget, enctype) <- generateFormPost uploadForm
  defaultLayout $ do
    setTitle "Upload"
    $(widgetFile "upload")

postUploadR :: Handler Html
postUploadR = do
  ((result, widget), enctype) <- runFormPost uploadForm
  case result of
   FormSuccess (imgFile, vidFile, title, tagsStr, desc, date) -> do
      imgFilename <- writeToServer imgFile True
      vidFilename <- writeToServer vidFile False
      let imgExt = takeExtension imgFilename
          vidExt = takeExtension vidFilename
          imgType = case imgExt of
                      ".png" -> VPng
                      ".jpg" -> VJpg
                      ".jpeg" -> VJpg
                      ".JPG" -> VJpg
                      ".JPEG" -> VJpg
                      _ -> VOther
          vidType = case vidExt of
                      ".webm" -> VWebm
                      ".mp4" -> VMp4
                      ".ogg" -> VOgg
                      _ -> VOgg
          tags = T.splitOn (" "::Text) tagsStr

      mtags' <- mapM (\t -> runDB $ selectFirst [TagName ==. t] []) tags
      liftIO $ print tags
      liftIO $ print mtags'

      let mtags :: [TagId]
          mtags = map (\(Entity tid _) -> tid) . L.map fromJust $ L.filter isJust mtags'
          vidTags = mtags

      if null vidTags
       then do
        setMessage "Wrong tags"
        redirect UploadR
       else do
        Just aui <- maybeAuthId
        vidId <- runDB $ insert (Video 0 0 0 aui date VActive title False (Just imgType) (Just imgFilename) vidType 300 vidFilename 0 vidTags (length vidTags) desc False)
        _ <- mapM_ (\t -> runDB $ do
                          _ <- insert (VideoTag vidId t)
                          update t [ TagUpdatedAt =. date
                                   , TagVideoCount +=. 1]) vidTags
        setMessage "Video saved"
        redirect UploadR
   _ -> do
     setMessage "Something went wrong"
     redirect UploadR

writeToServer :: FileInfo -> Bool -> Handler FilePath
writeToServer file imgMode = do
  let filename = unpack $ fileName file
      path = (if imgMode then imageFilePath else videoFilePath) filename
  liftIO $ fileMove file path
  return filename

imageFilePath :: String -> FilePath
imageFilePath f = imageUploadDir </> f

videoFilePath :: String -> FilePath
videoFilePath f = videoUploadDir </> f
