{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE RecordWildCards #-}
module Handler.Ban where

import           Data.Time
import           Import
import           Yesod.Form.Jquery

getBanR :: Handler Html
getBanR = do
  (widget, enctype) <- generateFormPost banForm
  defaultLayout $(widgetFile "ban")

data BanForm = BanForm { userId         :: Maybe UserId
                       , userIp         :: Text
                       , reason         :: Text
                       , expirationDate :: Day
                       , expirationTime :: TimeOfDay
                       }

banForm :: Form BanForm
banForm = renderDivs $ BanForm
  <$> pure Nothing
  <*> areq textField "User ip" Nothing
  <*> areq textField "Reason" Nothing
  <*> areq (jqueryDayField def
      { jdsChangeYear = True
      , jdsYearRange = "0:+1000"
      }) "Till date" Nothing
  <*> areq timeField "Till time" Nothing

postBanR :: Handler Html
postBanR = do
  ((result, widget), enctype) <- runFormPost banForm
  case result of
    FormSuccess BanForm{..} -> do
      curTime <- liftIO getCurrentTime
      let diffTime = timeOfDayToTime expirationTime
      let banExpiration = UTCTime expirationDate diffTime
      runDB $ insert (Ban userId (Just userIp) reason curTime curTime banExpiration)
      defaultLayout $ [whamlet|#{userIp} banned till: #{show banExpiration}|]
