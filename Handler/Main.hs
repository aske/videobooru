{-# LANGUAGE QuasiQuotes #-}
module Handler.Main where

import           CustomWidgets
import           FixedPaginator
import           Import

getMainR :: Handler Html
getMainR = do
  -- videos' <- runDB $ selectList [] [LimitTo 50, Desc VideoCreatedAt]
  -- (videos, paginWidget) <- paginate 9 videos'
  (videos, paginWidget) <- runDB $ selectPaginated 9 [] [Desc VideoCreatedAt]
  defaultLayout $ do
    setTitle "Temporary main page for auth"
    $(widgetFile "main")


