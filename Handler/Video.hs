module Handler.Video where

import           CustomWidgets
import           Data.List          ((!!))
import qualified Data.List          as L
import           Data.Maybe
import           Data.Text          (pack, takeWhile)
import           Database.Esqueleto as E
import           FixedPaginator
import           Foundation
import           Import             as I
import           Network.Wai
import           Yesod.Auth

-- getRemoteIp :: MonadHandler f => f String
-- getRemoteIp = L.takeWhile (/= ':') . show . remoteHost . reqWaiRequest <$> getRequest

commentForm :: Form Textarea
commentForm = renderBootstrap $ id
  <$> areq textareaField "Comment" Nothing

getVideoR :: VideoId -> Handler Html
getVideoR videoId = do
  video <- runDB $ get404 videoId
  let previewFile = maybe "img/no-preview.png" ("preview/"++) (videoPreviewpath video)
  let videoFormat = case videoFiletype video of
                      VWebm -> "video/webm" :: Text
                      VMp4 -> "video/mp4" :: Text
                      VOgg -> "video/ogg" :: Text
  (widget, enctype) <- generateFormPost commentForm
  maui <- maybeAuthId
  -- userIpAddress <- getRemoteIp

  comments <- runDB $ selectList [VideoCommentVideoId I.==. videoId] [Asc VideoCommentCreatedAt]

  let query = E.select $ E.from $ \(c `InnerJoin` vc) -> do
               on ((E.just (c ^. UserId)) E.==. vc E.^. VideoCommentCommenterId)
               where_ (vc ^. VideoCommentVideoId E.==. (val videoId))
               orderBy [asc (vc ^. VideoCommentCreatedAt)]
               return c

  liftIO $ print "VIDEOSGET"
  usersq <- runDB query
  let names :: [Text]
      names = map (\(Entity _ user) -> userName user) usersq
      (extnames, _) = foldl (\(acc, i) (Entity _ comment)  ->
                         let cid = videoCommentCommenterId comment
                         in if Data.Maybe.isNothing cid
                            then (acc ++ [names !! i], i)
                            else (acc ++ [names !! i], i+1)) ([], 0) comments

  -- (comms, paginWidget) <- runDB $ paginate 10 comments

  let cuids = zip comments ([0..(length comments)-1] :: [Int])

  defaultLayout $ do
    setTitle "Temporary main page for auth"
    $(widgetFile "video")
