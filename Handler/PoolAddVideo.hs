{-# LANGUAGE QuasiQuotes #-}
module Handler.PoolAddVideo where

import qualified Data.Text      as T
import           Data.Time
import           FixedPaginator
import           Import
import           Yesod.Auth

getPoolAddVideoR :: PoolId -> Handler Html
getPoolAddVideoR poolId = do
  (videos, paginWidget) <- runDB $ selectPaginated 10 [] [Asc VideoId]
  defaultLayout $ do
    setTitle "Add new video to the pool"
    $(widgetFile "pool-add-video")


postPoolAddVideoR :: PoolId -> Handler Html
postPoolAddVideoR poolId = do
  (vidtxt, deltxt) <- runInputPost $ (,) <$> ireq textField "vid"
                                        <*> ireq textField "deluid"
  let vid :: VideoId
      vid = read . T.unpack $ vidtxt
  curTime <- liftIO getCurrentTime
  Just uid <- maybeAuthId
  prevVid <- runDB $ selectFirst [ PoolVideosPoolId ==. poolId
                                , PoolVideosVideoId ==. vid] []
  case (prevVid, deltxt) of
    (Nothing, "add") -> do
      runDB $ do _ <- insert (PoolVideos poolId vid uid curTime)
                 update poolId [ PoolVideoCount +=. 1
                               , PoolUpdatedAt =. curTime
                               , PoolUpdatorId =. uid
                               ]
      setMessage "Successfully added"
      redirect $ PoolAddVideoR poolId
    (Nothing, "delete") -> do
      setMessage "Video is not in the pool"
      redirect $ PoolAddVideoR poolId
    (Just _, "add") -> do
        setMessage "Already added"
        redirect $ PoolAddVideoR poolId
    (Just _, "delete") -> do
        runDB $ deleteWhere [ PoolVideosPoolId ==. poolId
                            , PoolVideosVideoId ==. vid]
        setMessage "Deleted video from pool"
        redirect $ PoolAddVideoR poolId
