{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}
module Handler.Register where

import           Data.Text
import           Data.Time
import           Import
import           Yesod.Auth
import           Yesod.Form.Jquery


getRegisterR :: Handler Html
getRegisterR = do
  (widget, enctype) <- generateFormPost userForm
  defaultLayout $(widgetFile "register")

data UserForm = UserForm { name  :: Text
                         , email :: Text
                         }

userForm :: Form UserForm
userForm = renderBootstrap $ UserForm
  <$> areq uniqNameField "Username" Nothing
  -- email is not unique for testing purposes
  <*> areq emailField "Email" Nothing
  where
    uniqNameField = checkM (checkUniqName) textField
    checkUniqName name = do
      n <- runDB $ getBy $ UniqueUsername name
      case n of
        Just _ -> return $ Left ("Username is already taken"::Text)
        _ -> return $ Right name

postRegisterR :: Handler Html
postRegisterR = do
  ((result, widget), enctype) <- runFormPost userForm
  case result of
    FormSuccess UserForm{..} -> do
      curTime <- liftIO getCurrentTime
      curTz <- pack . show <$> liftIO getCurrentTimeZone
      runDB $ insert (User name "" (Just email) False curTz curTime Nothing)
      -- runDB $ insert (Ban userId (Just userIp) reason curTime curTime banExpiration)
      redirect $ AuthR LoginR
    FormMissing -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
          Form missing
          <a href=@{RegisterR}>Try again
      |]
    FormFailure (err:_) -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
            #{err}
        <p>
          <a href=@{RegisterR}>Try again
      |]
