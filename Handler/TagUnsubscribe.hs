module Handler.TagUnsubscribe where

import           Data.Time
import           Import
import           Yesod.Auth

getTagUnsubscribeR :: TagId -> Handler Html
getTagUnsubscribeR tagId = do
  tag <- runDB $ get404 tagId
  Just aui <- maybeAuthId
  prevSub <- runDB $ selectFirst [ TagSubscriptionSubscriberId ==. aui
                                , TagSubscriptionTagId  ==. tagId] []

  curTime <- liftIO getCurrentTime
  case prevSub of
    Nothing -> do
       setMessage "No subscription to delete"
       redirect $ TagR tagId
    Just (Entity sid _) -> do
      _ <- runDB $ delete sid
      redirect $ TagR tagId
