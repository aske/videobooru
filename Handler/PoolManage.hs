module Handler.PoolManage where

import qualified Data.Text      as T
import           Data.Time
import           FixedPaginator
import           Import
import           Yesod.Auth

getPoolManageR :: PoolId -> Handler Html
getPoolManageR poolId = do
  (users, paginWidget) <- runDB $ selectPaginated 10 [] [Asc UserId]
  defaultLayout $ do
    setTitle "Add user to the pool members"
    $(widgetFile "pool-manage")

postPoolManageR :: PoolId -> Handler Html
postPoolManageR poolId = do
  (usrtxt, deltxt) <- runInputPost $ (,) <$> ireq textField "uid"
                                        <*> ireq textField "deluid"
  let memberid :: UserId
      memberid = read . T.unpack $ usrtxt
  curTime <- liftIO getCurrentTime
  Just uid <- maybeAuthId
  prevVid <- runDB $ selectFirst [ PoolUsersPoolId ==. poolId
                                , PoolUsersUserId ==. memberid] []
  case (prevVid, deltxt) of
    (Nothing, "add") -> do
      runDB $ do _ <- insert (PoolUsers poolId memberid PoolMember)
                 update poolId [ PoolUpdatedAt =. curTime
                               , PoolUpdatorId =. uid
                               ]
      setMessage "Successfully added"
      redirect $ PoolManageR poolId
    (Nothing, "delete") -> do
      setMessage "User is not a pool member"
      redirect $ PoolManageR poolId
    (Just _, "add") -> do
        setMessage "Already added"
        redirect $ PoolManageR poolId
    (Just (Entity ouid _) , "delete") -> do
        runDB $ deleteWhere [ PoolUsersPoolId ==. poolId
                            , PoolUsersUserId ==. memberid]
        setMessage "Deleted user from pool members"
        redirect $ PoolManageR poolId
