{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}
module Handler.NewPool where

import           Data.Text
import           Data.Time
import           Import
import           Yesod.Auth
import           Yesod.Form.Jquery


getNewPoolR :: Handler Html
getNewPoolR = do
  (widget, enctype) <- generateFormPost poolForm
  defaultLayout $(widgetFile "new-pool")

data PoolForm = PoolForm { name        :: Text
                         , description :: Maybe Textarea
                         }

poolForm :: Form PoolForm
poolForm = renderBootstrap $ PoolForm
  <$> areq uniqNameField "Pool name" Nothing
  <*> aopt textareaField "Description" Nothing
  where
    uniqNameField = checkM (checkUniqName) textField
    checkUniqName name = do
      n <- runDB $ getBy $ UniquePoolname name
      case n of
        Just _ -> return $ Left ("Pool name is already taken"::Text)
        _ -> return $ Right name

postNewPoolR :: Handler Html
postNewPoolR = do
  ((result, widget), enctype) <- runFormPost poolForm
  -- shouldn't be Nothing ever
  Just uid <- maybeAuthId
  case result of
    FormSuccess PoolForm{..} -> do
      curTime <- liftIO getCurrentTime
      poolId <- runDB $ insert (Pool name uid curTime uid curTime description False False False 0)
      runDB $ insert (PoolUsers poolId uid PoolAdmin)
      defaultLayout $ do
        [whamlet|
          <h1>
            Success
            <p>
                New pool: #{name}
        |]
    FormMissing -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
          Form missing
          <a href=@{NewPoolR}>Try again
      |]
    FormFailure (err:_) -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
            #{err}
        <p>
          <a href=@{NewPoolR}>Try again
      |]
