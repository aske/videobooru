{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Handler.RecentPoolVideos where

import           CustomWidgets
import           Database.Esqueleto as E
import           FixedPaginator
import           Import
import           Yesod.Auth

getRecentPoolVideosR :: PoolId -> Handler Html
getRecentPoolVideosR poolId = do
  pool <- runDB $ get404 poolId
  let query = E.select $ E.from $ \(v `InnerJoin` pv) -> do
               on ((v ^. VideoId) E.==. (pv ^. PoolVideosVideoId))
               where_ (pv ^. PoolVideosPoolId E.==. (val poolId))
               orderBy [asc (v ^. VideoCreatedAt)]
               return v

  videos' <- runDB query
  muid <- maybeAuthId
  (videos, paginWidget) <- runDB $ paginate 9 videos'
  subscriptionsLayout $(widgetFile "recent-pool-videos")
