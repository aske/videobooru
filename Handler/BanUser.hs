{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE RecordWildCards #-}
module Handler.BanUser where

import           Data.Time
import           Import
import           Yesod.Form.Jquery

getBanUserR :: UserId -> Handler Html
getBanUserR userid = do
  (widget, enctype) <- generateFormPost (banForm userid)
  defaultLayout $(widgetFile "banuser")

data BanForm = BanForm { userId         :: Maybe UserId
                       , userIp         :: Maybe Text
                       , reason         :: Text
                       , expirationDate :: Day
                       , expirationTime :: TimeOfDay
                       }

banForm :: UserId -> Form BanForm
banForm userid = renderDivs $ BanForm
  <$> pure (Just userid)
  <*> aopt textField "User ip" Nothing
  <*> areq textField "Reason" Nothing
  <*> areq (jqueryDayField def
      { jdsChangeYear = True
      , jdsYearRange = "0:+1000"
      }) "Till date" Nothing
  <*> areq timeField "Till time" Nothing

postBanUserR :: UserId -> Handler Html
postBanUserR userid = do
  ((result, widget), enctype) <- runFormPost (banForm userid)
  case result of
    FormSuccess BanForm{..} -> do
      curTime <- liftIO getCurrentTime
      let diffTime = timeOfDayToTime expirationTime
      let banExpiration = UTCTime expirationDate diffTime
      runDB $ insert (Ban userId userIp reason curTime curTime banExpiration)
      defaultLayout $ [whamlet|Banned till: #{show banExpiration}|]
