module Handler.VideoVoteUp where

import           Data.Time
import           Import
import           Yesod.Auth

getVideoVoteUpR :: VideoId -> Handler Html
getVideoVoteUpR videoId = do
  video <- runDB $ get404 videoId
  liftIO $ print video
  Just aui <- maybeAuthId
  prevVote <- runDB $ selectFirst [VideoVotesVoterId ==. aui
                                 , VideoVotesVideoId ==. videoId] []
  curTime <- liftIO getCurrentTime
  case prevVote of
    Nothing -> do
       _ <- runDB $ update videoId [VideoScore +=. 1, VideoUpVotes +=. 1]
       _ <- runDB $ insert $ VideoVotes videoId aui curTime curTime VideoUp
       redirect $ VideoR videoId
    Just (Entity voteid (VideoVotes _ _ _ _ VideoUp)) -> redirect $ VideoR videoId
    Just (Entity voteid (VideoVotes _ _ _ _ VideoDown)) -> do
      _ <- runDB $ update voteid [ VideoVotesUpdatedAt =. curTime
                                , VideoVotesVote =. VideoUp]
      _ <- runDB $ update videoId [VideoScore +=. 2, VideoUpVotes +=. 1, VideoDownVotes -=. 1]
      redirect $ VideoR videoId
