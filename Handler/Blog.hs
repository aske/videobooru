{-# LANGUAGE QuasiQuotes #-}
module Handler.Blog where

import           Data.Time
import           Import

newtype UserId = UserId Int deriving Show

data Blog = Blog { blogTitle    :: Text
                 , blogContents :: Textarea
                 , blogUser     :: Handler.Blog.UserId
                 , blogPosted   :: UTCTime
                 } deriving Show

blogForm :: Handler.Blog.UserId -> Form Blog
blogForm userId = renderDivs $ Blog
  <$> areq textField "Title" Nothing
  <*> areq textareaField "Contents" Nothing
  <*> pure userId
  <*> lift (liftIO getCurrentTime)

getBlogR :: Handler Html
getBlogR = do
  let userId = Handler.Blog.UserId 5 -- todo auth
  ((res, widget), enctype) <- runFormPost $ blogForm userId
  defaultLayout
    [whamlet|
        <p>Previous result: #{show res}
        <form method=post action=@{BlogR} enctype=#{enctype}>
              ^{widget}
              <input type=submit>
    |]


postBlogR :: Handler Html
postBlogR = getBlogR
