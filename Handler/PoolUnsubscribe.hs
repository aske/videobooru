module Handler.PoolUnsubscribe where

import           Data.Time
import           Import
import           Yesod.Auth

getPoolUnsubscribeR :: PoolId -> Handler Html
getPoolUnsubscribeR poolId = do
  pool <- runDB $ get404 poolId
  Just aui <- maybeAuthId
  prevSub <- runDB $ selectFirst [ PoolSubscriptionSubscriberId ==. aui
                                , PoolSubscriptionPoolId  ==. poolId] []

  curTime <- liftIO getCurrentTime
  case prevSub of
    Nothing -> do
       setMessage "No subscription to delete"
       redirect $ PoolR poolId
    Just (Entity sid _) -> do
      _ <- runDB $ delete sid
      redirect $ PoolR poolId
