{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}
module Handler.NewTag where

import           Data.Text
import           Data.Time
import           Import
import           Yesod.Auth
import           Yesod.Form.Jquery

getNewTagR :: Handler Html
getNewTagR = do
  (widget, enctype) <- generateFormPost tagForm
  defaultLayout $(widgetFile "new-tag")


data TagForm = TagForm { name        :: Text
                       , description :: Maybe Textarea
                       }

tagForm :: Form TagForm
tagForm = renderBootstrap $ TagForm
  <$> areq uniqNameField "Tag name" Nothing
  <*> aopt textareaField "Description" Nothing
  where
    uniqNameField = checkM (checkUniqName) textField
    checkUniqName name = do
      n <- runDB $ getBy $ UniqueTagname name
      case n of
        Just _ -> return $ Left ("Tag name is already taken"::Text)
        _ -> return $ Right name

postNewTagR :: Handler Html
postNewTagR = do
  ((result, widget), enctype) <- runFormPost tagForm
  -- shouldn't be Nothing ever
  Just uid <- maybeAuthId
  case result of
    FormSuccess TagForm{..} -> do
      curTime <- liftIO getCurrentTime
      runDB $ insert (Tag name 0 curTime uid description curTime uid)
      defaultLayout $ do
        [whamlet|
          <h1>
            Success
            <p>
                New tag: #{name}
        |]
    FormMissing -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
          Form missing
          <a href=@{NewTagR}>Try again
      |]
    FormFailure (err:_) -> defaultLayout $ do
      [whamlet|
        <h1>Error
        <p>
            #{err}
        <p>
          <a href=@{NewTagR}>Try again
      |]
