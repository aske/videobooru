{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module Handler.Person where

import           Control.Arrow     ((&&&))
import           Data.Text         (pack)
import           Data.Time
import           Import
import           Yesod.Form.Jquery

data Person = Person { personName          :: Text
                     , personBirthday      :: Day
                     , personBirthtime     :: TimeOfDay
                     , personFavoriteColor :: Maybe Color
                     , personEmail         :: Text
                     , personWebsite       :: Maybe Text
                     } deriving Show

data Color = Red | Green | Blue | Yellow
  deriving (Show, Eq, Enum, Bounded)

personForm :: Form Person
personForm = renderDivs $ Person
  <$> areq personNameField "Name" Nothing
  <*> areq (jqueryDayField def
      { jdsChangeYear = True
      , jdsYearRange = "1900:-5"
      }) "Birthday" Nothing
  <*> areq timeField "Birth time" Nothing
  <*> aopt (selectFieldList colors) "Favorite color" Nothing
  <*> areq emailField "Email address" Nothing
  <*> aopt urlField "Website" Nothing
  where
    errorMessage = "Gay is not a color!" :: Text
    personNameField = checkBool (/= "Mako") errorMessage textField
    colors = map (pack . show &&& id) [minBound..maxBound]

getPersonR :: Handler Html
getPersonR = do
  (widget, enctype) <- generateFormPost personForm
  defaultLayout $(widgetFile "person")

postPersonR :: Handler Html
postPersonR = do
  ((result, widget), enctype) <- runFormPost personForm
  case result of
       FormSuccess person -> defaultLayout [whamlet|<p>#{show person}|]
       _ -> defaultLayout
         [whamlet|
            <p>Invalid input, try again.
            <form method=post action=@{PersonR} enctypt=#{enctype}>
                  ^{widget}
                  <button>Submit
         |]

