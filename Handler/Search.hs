{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Handler.Search where

import           CustomWidgets
import qualified Data.List          as L
import           Data.Maybe
import           Data.Text          as T (splitOn)
import           Database.Esqueleto as E
import           FixedPaginator
import           Import             as I

getSearchR :: Handler Html
getSearchR = do
  tagsText <- runInputGet $ id <$> ireq textField "tag-search"
  let tags :: [Text]
      tags = T.splitOn (" "::Text) tagsText

  mtags' <- mapM (\t -> runDB $ selectFirst [TagName I.==. t] []) tags

  let mtags :: [TagId]
      mtags = map (\(Entity tid _) -> tid) . L.map fromJust $ L.filter isJust mtags'

      smallExists tid vt = where_ $ exists $
                                    E.from $ \a -> do
                                      where_ (a ^. VideoTagVideoId E.==. (vt ^. VideoTagVideoId))
                                      where_ (a ^. VideoTagTagId E.==. (val tid))

      smallChains vt = L.map (\tid -> smallExists tid vt) mtags
      bigExistsChain vt = L.foldl1 (\acc ch -> do {acc; ch}) (smallChains vt)

  let query =
                E.select $ E.from $ \(v `InnerJoin` vt `InnerJoin` t) -> do
                  on ((vt ^. VideoTagTagId) E.==. (t ^. TagId))
                  on ((vt ^. VideoTagVideoId) E.==. (v ^. VideoId))
                  orderBy [asc (v ^. VideoCreatedAt)]
                  bigExistsChain vt
                  return v
{-
select video_id from video_tag
  where exists
  (select * from video_tag as a where a.video_id = video_tag.video_id and tag_id = 2)
   and exists
    (select video_id from video_tag as a where a.video_id = video_tag.video_id and tag_id = 4);
-}

  q' <- runDB query
  liftIO $ print q'
  (videos, paginWidget) <- runDB $ paginate 4 (L.nub q')
  -- (videos, paginWidget) <- runDB $ paginate 4 q'
  defaultLayout $(widgetFile "search")
