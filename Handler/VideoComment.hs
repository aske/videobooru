module Handler.VideoComment where

import qualified Data.Text  as T
import           Data.Time
import           Import
import           Yesod.Auth

getVideoCommentR :: VideoId -> Handler Html
getVideoCommentR = error "Not yet implemented: getVideoCommentR"

commentForm :: Form Textarea
commentForm = renderBootstrap $ id
  <$> areq textareaField "Comment" Nothing

postVideoCommentR :: VideoId -> Handler Html
postVideoCommentR videoId = do
  maui <- maybeAuthId
  ((result, _), _) <- runFormPost commentForm
  curTime <- liftIO getCurrentTime
  curIp <- T.pack <$> getRemoteIp
  case result of
    FormSuccess comment -> do
      liftIO $ print "success"
      cid <- runDB $ insert $ VideoComment videoId maui curTime comment False curIp 0 0 0
      redirect $ VideoR videoId
    _ -> do
      liftIO $ print "error"
      redirect $ VideoR videoId
