{-# LANGUAGE QuasiQuotes #-}
module Handler.SetGreeting where

import           Import

getSetGreetingR :: Handler Html
getSetGreetingR = defaultLayout
  [whamlet|
    <form method=post>
          Greet me as #
          <input type=text name=name>
          . #
          <input type=submit value="Set name">
  |]

postSetGreetingR :: Handler ()
postSetGreetingR = do
  name <- runInputPost $ ireq textField "name"
  setSession "name" name
  redirectUltDest HomeR
