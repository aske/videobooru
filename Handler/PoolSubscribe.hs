module Handler.PoolSubscribe where

import           Data.Time
import           Import
import           Yesod.Auth


getPoolSubscribeR :: PoolId -> Handler Html
getPoolSubscribeR poolId = do
  pool <- runDB $ get404 poolId
  Just aui <- maybeAuthId
  prevSub <- runDB $ selectFirst [ PoolSubscriptionSubscriberId ==. aui
                                , PoolSubscriptionPoolId  ==. poolId] []

  curTime <- liftIO getCurrentTime
  case prevSub of
    Nothing -> do
       _ <- runDB $ insert $ PoolSubscription aui poolId curTime
       redirect $ PoolR poolId
    Just _ -> do
      setMessage "Already subscribed"
      redirect $ PoolR poolId
