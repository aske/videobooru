module Handler.Pools where

import           Import

import           FixedPaginator

getPoolsR :: Handler Html
getPoolsR = do
  (pools, paginWidget) <- runDB $ selectPaginated 10 [] [Asc PoolId]
  defaultLayout $ do
    setTitle "Pools"
    $(widgetFile "pools")
