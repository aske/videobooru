{-# LANGUAGE TemplateHaskell #-}
module Handler.Users where

import           CustomWidgets
import           FixedPaginator
import           Import

getUsersR :: Handler Html
getUsersR = do
  (users, paginWidget) <- runDB $ selectPaginated 10 [] [Asc UserId]
  defaultLayout $ do
    setTitle "Users"
    $(widgetFile "users")
