module Handler.TagSubscribe where

import           Data.Time
import           Import
import           Yesod.Auth

getTagSubscribeR :: TagId -> Handler Html
getTagSubscribeR tagId = do
  tag <- runDB $ get404 tagId
  Just aui <- maybeAuthId
  prevSub <- runDB $ selectFirst [ TagSubscriptionSubscriberId ==. aui
                                , TagSubscriptionTagId  ==. tagId] []

  curTime <- liftIO getCurrentTime
  case prevSub of
    Nothing -> do
       _ <- runDB $ insert $ TagSubscription aui tagId curTime
       redirect $ TagR tagId
    Just _ -> do
      setMessage "Already subscribed"
      redirect $ TagR tagId
