{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE TemplateHaskell #-}
module Application
    ( makeApplication
    , getApplicationDev
    , makeFoundation
    ) where

import           Control.Monad.Logger                 (runLoggingT)
import           Data.Default                         (def)
import qualified Database.Persist
import           Database.Persist.Sql                 (runMigration)
import qualified GHC.IO.FD
import           Import
import           Network.HTTP.Conduit                 (conduitManagerSettings,
                                                       newManager)
import           Network.Wai.Logger                   (clockDateCacher)
import           Network.Wai.Middleware.RequestLogger (IPAddrSource (..),
                                                       OutputFormat (..),
                                                       destination,
                                                       mkRequestLogger,
                                                       outputFormat)
import qualified Network.Wai.Middleware.RequestLogger as RequestLogger
import           Settings
import           System.Log.FastLogger                (defaultBufSize,
                                                       newLoggerSet)
import           Yesod.Auth
import           Yesod.Core.Types                     (Logger (Logger),
                                                       loggerSet)
import           Yesod.Default.Config
import           Yesod.Default.Handlers
import           Yesod.Default.Main

-- Import all relevant handler modules here.
-- Don't forget to add new modules to your cabal file!
import           Handler.Admin
import           Handler.Ban
import           Handler.BanUser
import           Handler.Blog
import           Handler.Favorite
import           Handler.Home
import           Handler.Just
import           Handler.Main
import           Handler.NewPool
import           Handler.NewTag
import           Handler.NewTag
import           Handler.Person
import           Handler.Pool
import           Handler.PoolAddVideo
import           Handler.PoolManage
import           Handler.Pools
import           Handler.PoolSubscribe
import           Handler.PoolUnsubscribe
import           Handler.Profile
import           Handler.RecentPoolVideos
import           Handler.RecentTagVideos
import           Handler.RedirectTest
import           Handler.Register
import           Handler.Search
import           Handler.SetGreeting
import           Handler.Tag
import           Handler.TagSubscribe
import           Handler.TagUnsubscribe
import           Handler.Upload
import           Handler.User
import           Handler.Users
import           Handler.Video
import           Handler.VideoComment
import           Handler.Videos
import           Handler.VideoVote
import           Handler.VideoVoteDown
import           Handler.VideoVoteUp

-- This line actually creates our YesodDispatch instance. It is the second half
-- of the call to mkYesodData which occurs in Foundation.hs. Please see the
-- comments there for more details.
mkYesodDispatch "App" resourcesApp

-- This function allocates resources (such as a database connection pool),
-- performs initialization and creates a WAI application. This is also the
-- place to put your migrate statements to have automatic database
-- migrations handled by Yesod.
makeApplication :: AppConfig DefaultEnv Extra -> IO Application
makeApplication conf = do
    foundation <- makeFoundation conf

    -- Initialize the logging middleware
    logWare <- mkRequestLogger def
        { outputFormat =
            if development
                then Detailed True
                else Apache FromSocket
        , destination = RequestLogger.Logger $ loggerSet $ appLogger foundation
        }

    -- Create the WAI application and apply middlewares
    app <- toWaiAppPlain foundation
    return $ logWare app

-- | Loads up any necessary settings, creates your foundation datatype, and
-- performs some initialization.
makeFoundation :: AppConfig DefaultEnv Extra -> IO App
makeFoundation conf = do
    manager <- newManager conduitManagerSettings
    s <- staticSite
    dbconf <- withYamlEnvironment "config/postgresql.yml" (appEnv conf)
              Database.Persist.loadConfig >>=
              Database.Persist.applyEnv
    p <- Database.Persist.createPoolConfig (dbconf :: Settings.PersistConf)

    loggerSet' <- newLoggerSet defaultBufSize GHC.IO.FD.stdout
    (getter, _) <- clockDateCacher

    let logger = Yesod.Core.Types.Logger loggerSet' getter
        foundation = App conf s p manager dbconf logger

    -- Perform database migration using our application's logging settings.
    runLoggingT
        (Database.Persist.runPool dbconf (runMigration migrateAll) p)
        (messageLoggerSource foundation logger)

    return foundation

-- for yesod devel
getApplicationDev :: IO (Int, Application)
getApplicationDev =
    defaultDevelApp loader makeApplication
  where
    loader = Yesod.Default.Config.loadConfig (configSettings Development)
        { csParseExtra = parseExtra
        }
