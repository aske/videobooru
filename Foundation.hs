{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies    #-}
module Foundation where

import           Data.Functor           ((<$>))
import           Data.Text              (Text)
import qualified Data.Text              as T
import           Data.Time              (getCurrentTime)
import qualified Database.Persist
import           Database.Persist.Sql   (SqlPersistT)
import           Model
import           ModelTypes
import           Network.HTTP.Conduit   (Manager)
import           Network.Wai
import           Prelude
import           Settings               (Extra (..), widgetFile)
import qualified Settings
import           Settings.Development   (development)
import           Settings.StaticFiles
import           Text.Hamlet            (hamletFile)
import           Text.Jasmine           (minifym)
import           Yesod
import           Yesod.Auth
import           Yesod.Auth.BrowserId
import           Yesod.Auth.GoogleEmail
import           Yesod.Core.Types       (Logger)
import           Yesod.Default.Config
import           Yesod.Default.Util     (addStaticContentExternal)
import           Yesod.Form.Jquery
import           Yesod.Static

-- | The site argument for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { settings      :: AppConfig DefaultEnv Extra
    , getStatic     :: Static -- ^ Settings for static file serving.
    , connPool      :: Database.Persist.PersistConfigPool Settings.PersistConf -- ^ Database connection pool.
    , httpManager   :: Manager
    , persistConfig :: Settings.PersistConf
    , appLogger     :: Logger
    }

-- Set up i18n messages. See the message folder.
mkMessage "App" "messages" "en"

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- http://www.yesodweb.com/book/routing-and-handlers
--
-- Note that this is really half the story; in Application.hs, mkYesodDispatch
-- generates the rest of the code. Please see the linked documentation for an
-- explanation for this split.
mkYesodData "App" $(parseRoutesFile "config/routes")

type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.
instance Yesod App where
    approot = ApprootMaster $ appRoot . settings

    -- Store session data on the client in encrypted cookies,
    -- default session idle timeout is 120 minutes
    makeSessionBackend _ = fmap Just $ defaultClientSessionBackend
        (120 * 60) -- 120 minutes
        "config/client_session_key.aes"

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage
        maui <- maybeAuthId

        -- We break up the default layout into two components:
        -- default-layout is the contents of the body tag, and
        -- default-layout-wrapper is the entire page. Since the final
        -- value passed to hamletToRepHtml cannot be a widget, this allows
        -- you to use normal widget features in default-layout.

        pc <- widgetToPageContent $ do
            $(combineStylesheets 'StaticR
                [ css_normalize_css
                , css_bootstrap_css
                ])
            $(widgetFile "default-layout")
        giveUrlRenderer $(hamletFile "templates/default-layout-wrapper.hamlet")

    -- This is done to provide an optimization for serving static files from
    -- a separate domain. Please see the staticRoot setting in Settings.hs
    urlRenderOverride y (StaticR s) =
        Just $ uncurry (joinPath y (Settings.staticRoot $ settings y)) $ renderRoute s
    urlRenderOverride _ _ = Nothing

    -- The page to be redirected to when authentication is required.
    authRoute _ = Just $ AuthR LoginR

    -- This function creates static content files in the static folder
    -- and names them based on a hash of their content. This allows
    -- expiration dates to be set far in the future without worry of
    -- users receiving stale content.
    addStaticContent =
        addStaticContentExternal minifym genFileName Settings.staticDir (StaticR . flip StaticRoute [])
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs
            | development = "autogen-" ++ base64md5 lbs
            | otherwise   = base64md5 lbs

    -- Place Javascript at bottom of the body tag so the rest of the page loads first
    jsLoader _ = BottomOfBody

    -- What messages should be logged. The following includes all messages when
    -- in development, and warnings and errors in production.
    shouldLog _ _source level =
        development || level == LevelWarn || level == LevelError

    makeLogger = return . appLogger

    errorHandler NotFound = fmap toTypedContent $ defaultLayout $ do
      setTitle "Requested page not found"
      $(widgetFile "404")
    errorHandler other = defaultErrorHandler other

    maximumContentLength _ (Just UploadR) = Just $ 2 * 1024 * 1024 * 1024 -- 2 GiB
    maximumContentLength _ _ = Just $ 2 * 1024 * 1024 -- 2 MiB

    isAuthorized UsersR _ = isAdmin
    isAuthorized (UserR _) _ = isAdmin
    isAuthorized (BanUserR _) _ = isAdmin
    isAuthorized BanR _ = isAdmin
    isAuthorized AdminR _ = isAdmin
    isAuthorized UploadR _ = isUser
    isAuthorized NewTagR _ = isUser
    isAuthorized (VideoVoteUpR _) _ = isUser
    isAuthorized (VideoVoteDownR _) _ = isUser
    isAuthorized (VideoCommentR _) _ = notBanned
    isAuthorized (FavoriteR _) _ = isUser
    isAuthorized (TagSubscribeR _) _ = isUser
    isAuthorized (TagUnsubscribeR _) _ = isUser

    isAuthorized NewPoolR _ = isUser
    isAuthorized (PoolAddVideoR poolId) _ = isPoolMember poolId
    isAuthorized (PoolManageR poolId) _ = isPoolAdmin poolId

    isAuthorized (PoolSubscribeR _) _ = isUser
    isAuthorized (PoolUnsubscribeR _) _ = isUser

    isAuthorized (RecentTagVideosR _) _ = isUser

    isAuthorized _ _ = return Authorized

getRemoteIp :: MonadHandler f => f String
getRemoteIp = takeWhile (/= ':') . show . remoteHost . reqWaiRequest <$> getRequest

isPoolAdmin poolId = do
  mu <- maybeAuthId
  case mu of
    Nothing -> return AuthenticationRequired
    Just userid -> do
      poolUser <- runDB $ selectFirst [ PoolUsersUserId ==. userid
                                     , PoolUsersPoolId ==. poolId] []
      case poolUser of
        Just (Entity _ (PoolUsers _ _ PoolAdmin)) -> return Authorized
        _ -> return $ Unauthorized "You are not pool admin"

isPoolMember poolId = do
  mu <- maybeAuthId
  case mu of
    Nothing -> return AuthenticationRequired
    Just userid -> do
      poolUser <- runDB $ selectFirst [ PoolUsersUserId ==. userid
                                     , PoolUsersPoolId ==. poolId] []
      case poolUser of
        Just _ -> return Authorized
        _ -> return $ Unauthorized "You are not pool admin"

notBanned :: HandlerT App IO AuthResult
notBanned = do
  mu <- maybeAuthId
  case mu of
    Nothing -> do
      currentIp <- T.pack <$> getRemoteIp
      bans' <- runDB $ selectList [BanUserIp ==. Just currentIp] [Asc BanExpiresOn]
      curTime <- liftIO getCurrentTime
      let (expiredBans, actualBans) = span (\(Entity _ Ban{..}) -> banExpiresOn <= curTime) $ bans'
      _ <- mapM_ (\(Entity banid _) -> runDB $ delete banid) expiredBans
      if not . null $ actualBans
         then return $ Unauthorized "You are banned"
         else return  Authorized
    Just userid -> do
      bans' <- runDB $ selectList [BanUserId ==. Just userid] [Asc BanExpiresOn]
      curTime <- liftIO getCurrentTime
      let (expiredBans, actualBans) = span (\(Entity _ Ban{..}) -> banExpiresOn <= curTime) $ bans'
      _ <- mapM_ (\(Entity banid _) -> runDB $ delete banid) expiredBans
      liftIO $ print actualBans
      if not . null $ actualBans
         then return $ Unauthorized "You are banned"
         else return  Authorized

isUser :: HandlerT App IO AuthResult
isUser = do
  mu <- maybeAuthId
  case mu of
    Nothing -> return AuthenticationRequired
    Just userid -> do
      -- no check for ip bans, because anons can't upload
      bans' <- runDB $ selectList [BanUserId ==. Just userid] [Asc BanExpiresOn]
      curTime <- liftIO getCurrentTime
      let (expiredBans, actualBans) = span (\(Entity _ Ban{..}) -> banExpiresOn <= curTime) $ bans'
      _ <- mapM_ (\(Entity banid _) -> runDB $ delete banid) expiredBans
      if not . null $ actualBans
         then return $ Unauthorized "You are banned"
         else return  Authorized


isAdmin :: HandlerT App IO AuthResult
isAdmin = do
  mu <- maybeAuthId
  case mu of
    Nothing -> return AuthenticationRequired
    Just userid -> do
         euser <- runDB $ selectFirst [UserId ==. userid] []
         case euser of
           Just (Entity userid (User {..})) -> case userName of
                                                "admin" -> return Authorized
                                                _ -> return $ Unauthorized "Must be admin"
           Nothing -> return $ Unauthorized "You must be admin"

-- How to run database actions.
instance YesodPersist App where
    type YesodPersistBackend App = SqlPersistT
    runDB = defaultRunDB persistConfig connPool
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner connPool

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = HomeR

    getAuthId creds = runDB $ do
        let credentials = credsIdent creds
        x <- selectFirst [UserEmail ==. Just credentials] []
        liftIO $ print x
        case x of
            Just (Entity uid user) -> return $ Just uid
            _ -> return Nothing
                -- fmap Just $ insert $ User (credsIdent creds) ("12345"::Text) Nothing

    -- You can add other plugins like BrowserID, email or OAuth here
    authPlugins _ = [authBrowserId def, authGoogleEmail]

    authHttpManager = httpManager

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.
instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

instance YesodJquery App

-- | Get the 'Extra' value, used to hold data from the settings.yml file.
getExtra :: Handler Extra
getExtra = fmap (appExtra . settings) getYesod

-- Note: previous versions of the scaffolding included a deliver function to
-- send emails. Unfortunately, there are too many different options for us to
-- give a reasonable default. Instead, the information is available on the
-- wiki:
--
-- https://github.com/yesodweb/yesod/wiki/Sending-email
