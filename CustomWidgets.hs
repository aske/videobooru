{-# LANGUAGE QuasiQuotes     #-}
{-# LANGUAGE TemplateHaskell #-}
module CustomWidgets where

import           Import
import           Settings.StaticFiles
import           Text.Hamlet            (hamletFile)
import           Yesod.Default.Config
import           Yesod.Default.Handlers
import           Yesod.Default.Main

subscriptionsLayout widget = do
    master <- getYesod
    mmsg <- getMessage

    pc <- widgetToPageContent $ do
          $(combineStylesheets 'StaticR
             [ css_normalize_css
             , css_bootstrap_css
             ])
          $(widgetFile "subscriptions-layout")
    giveUrlRenderer $(hamletFile "templates/subscriptions-layout-wrapper.hamlet")

searchWidget :: Widget
searchWidget = do
  [whamlet|
    <form .form-search action=@{SearchR}>
      <div .input-append>
        <input .span2 .search-query type="text" placeholder="Search" name="tag-search">
        <button .btn type="submit">Go
  |]

tagsSidebar :: Widget -> Widget
tagsSidebar tagsWidget = do
  [whamlet|
      <div>
        <aside .sidebar>
            ^{searchWidget}
          <section>
            <h2>Tags
            ^{tagsWidget}
  |]

recentTagsBar :: Widget
recentTagsBar = do
  recentTags <- handlerToWidget $ runDB $ selectList [] [LimitTo 10, Desc TagUpdatedAt]
  [whamlet|
    <style>
        li {
           list-style-type: none;
        }
        ul {
           margin-left: 0;
           padding-left: 0;
        }
    <ul>
        $forall Entity tagid tag <- recentTags
           <li>
            <a href=@{TagR tagid}>#{tagName tag}
            <span .badge>#{tagVideoCount tag}
  |]

videoTagsBar :: Video -> Widget
videoTagsBar video = do
  let tagIds = videoTags video
  tags <- mapM (handlerToWidget . runDB . get404) tagIds
  [whamlet|
    <style>
        li {
           list-style-type: none;
        }
        ul {
           margin-left: 0;
           padding-left: 0;
        }
    <ul>
        $forall (tagid, tag) <- zip tagIds tags
           <li>
            <a href=@{TagR tagid}>#{tagName tag}
            <span .badge>#{tagVideoCount tag}
  |]
