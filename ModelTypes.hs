{-# LANGUAGE TemplateHaskell #-}
module ModelTypes where

import           Prelude
-- import           Yesod
import           Database.Persist.TH

data VideoStatus = VActive | VSuspended | VDeleted
     deriving (Show, Read, Eq)
derivePersistField "VideoStatus"

data VideoPreviewFiletype = VPng | VJpg | VOther
     deriving (Show, Read, Eq)
derivePersistField "VideoPreviewFiletype"

data VideoFiletype = VWebm | VMp4 | VOgg
     deriving (Show, Read, Eq)
derivePersistField "VideoFiletype"

data UploadStatus = UploadPending | UploadRejected | UploadAccepted
     deriving (Show, Read, Eq)
derivePersistField "UploadStatus"

data VideoVote = VideoUp | VideoDown
     deriving (Show, Read, Eq)
derivePersistField "VideoVote"

data CommentVote = CommentUp | CommentDown
     deriving (Show, Read, Eq)
derivePersistField "CommentVote"

data PoolRole = PoolAdmin | PoolMember
     deriving (Show, Read, Eq)
derivePersistField "PoolRole"
